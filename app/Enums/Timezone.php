<?php

namespace App\Enums;

use MabeEnum\Enum;

/**
 * @method static Timezone CET()
 * @method static Timezone CST()
 * @method static Timezone GMT1()
 */
class Timezone extends Enum
{
    const CET = 'CET';
    const CST = 'CST';
    const GMT1 = 'GMT+1';
}
