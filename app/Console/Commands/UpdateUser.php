<?php

namespace App\Console\Commands;

use App\Enums\Timezone;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class UpdateUser extends Command
{
    /**
     * @var string
     */
    protected $signature = 'update:user {user}';

    /**
     * @var string
     */
    protected $description = 'Updates a user with new random values';

    /**
     * @var Faker
     */
    protected $faker;

    public function __construct(Faker $faker)
    {
        parent::__construct();

        $this->faker = $faker;
    }

    public function handle(): void
    {
        $user = User::find($this->argument('user'));
        if (empty($user)) {
            return;
        }

        $user->update([
            'name' => $this->faker->name,
            'timezone' => Arr::random(Timezone::getValues())
        ]);
    }
}
