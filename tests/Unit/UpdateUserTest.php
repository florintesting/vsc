<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    use RefreshDatabase;

    public function test_update_user_command(): void
    {
        $user = factory(User::class)->create();

        $this->artisan('update:user', ['user' => 1]);

        $this->assertNotEquals($user->name, $user->fresh()->name);

        $user = $user->fresh();
        $this->artisan('update:user', ['user' => 2]);

        $this->assertEquals($user->name, $user->fresh()->name);
    }
}
