<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use UsersTableSeeder;

class UserSeederTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_seeder(): void
    {
        $this->seed(UsersTableSeeder::class);

        $this->assertEquals(20, User::count());
    }
}
